// console.log("Hello World");

// Exponent Operator
// befor es6
const firstNum = 8 ** 2;
console.log(firstNum);

// es6
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// template literals
let name = "John";
console.log(`Hell ${name}!
Welcome to programming!`);

const interestRate = 0.1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`)

// Array destructuring
const fullName = ["Juan", "Dela", "Cruz"];
const [firstName, middleName, lastName] = fullName;
console.log(`${firstName}`);
console.log(`${middleName}`);
console.log(`${lastName}`);
console.log((`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`));

// Object Destructuring
const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
}
const {givenName, maidenName, familyName} = person;
console.log(`${givenName}`);
console.log(`${maidenName}`);
console.log(`${familyName}`);

function getFullName({givenName, maidenName, familyName}) {
    console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);

// Arrow Functions

const hello = () => {
    console.log(`Hello World!`);
}

// const hello = function() {
//     console.log(`Hello World!`);
// }
hello()

function printFullName(firstName, middleInitial,lastName) {
    console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("Ronjon", "M.", "Nuno");

let fName = (firstName, middleInitial, lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`);
}

fName("Ronjon", "M.", "Nuno");

// Arrow functions with loops
const student = ["John", "Jane", "Judy"];

function iterate(student){
    console.log(`${student} is a student!`)
}
student.forEach(iterate)

student.forEach(student => {
    console.log(`${student}`)
});

// Implicit Return Statement
// no return
const add = (x,y) => {
    console.log(x+y);
}
console.log(add(10, 20));
// with return
const subtract = (x,y) => x-y;
console.log(subtract(10, 5));

// default function argument value
const greet = (name = "NoArgs") => {
    return `Goodmornig, ${name}`;
}
console.log(greet());

// Class-based Object blueprints

class Car{
    constructor (brand, name, year){
        this.carBrand = brand;
        this.carName = name;
        this.carYear = year;
    }
}
let car = new Car("Toyota", "Hilux-Pickip", "2015");
console.log(car);

car.carBrand = "Nissan";
console.log(car);
